import React from "react";
import { NavLink } from "react-router-dom";
import "./NavBar.css";

const NavBar = () => {
  return (
    <div className="NavBar-container">
      <ul className="ul-list">
        <li className="nav-link">
          {" "}
          <NavLink to="/im">Моя страница</NavLink>
        </li>
        <li className="nav-link">
          {" "}
          <NavLink to="/news">Новости</NavLink>
        </li>
        <li className="nav-link">
          {" "}
          <NavLink to="/messages">Сообщения</NavLink>
        </li>
        <li className="nav-link">
          {" "}
          <NavLink to="/friends">Друзья</NavLink>
        </li>
        <li className="nav-link">
          {" "}
          <NavLink to="/fotos">Фотографии</NavLink>
        </li>
        <br />
        <li className="nav-link">
          {" "}
          <NavLink to="/covid">
            Короновирус
          </NavLink>
        </li>
      </ul>
    </div>
  );
};
export default NavBar;
