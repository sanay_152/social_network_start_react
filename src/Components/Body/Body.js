import React from "react";
import NavBar from "./NavBar/NavBar";
import MyPageContainer from "./Content/MyPageContainer";
import Friends from "./Content/Friends/Friends";
import CovidInfo from '../../CovidInfo/CovidInfo'
import DialogsContainer from './Content/Dialogs/DialogsContainer'
import { Route } from "react-router-dom";
import "./Body.css";

class Body extends React.Component {
  render() {
    return (
      <div className="Body-own">
        <NavBar />
        <div className="spliter">
          <Route path="/im" render={() => <MyPageContainer />} />
          <Route path="/friends" render={() => <Friends />} />
          <Route path="/messages" render={() => <DialogsContainer />} />
          <Route path="/covid" render={() => <CovidInfo />} />
        </div>
      </div>
    );
  }
}
export default Body;
