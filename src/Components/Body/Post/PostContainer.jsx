import React from "react";
import { Post } from "./Post";
import "./Post.css";

export const PostContainer = (props) => {
  
  let CurrentPosts = props.allPosts.map((elem) => (
    <Post key={elem.id} state={elem} deletePost={props.deletePost} />
  ))

  return (
    <div className="postContainer">
      {CurrentPosts}
    </div>
  );
};

    
