import React from "react";
import "./Post.css";
import miniava from '../../../img/ava.jpg'

export const Post = ({ state: { sername, name, body, date, id }, deletePost }) => {
  return (
      <div  className="post-container">
        <div className='postTools'>
        <input id={id} type="button" className="deleteButton" value="&#9998;" onClick={deletePost}/>
           <input id={id} type="button" className="editButton" value="&times;" onClick={deletePost}/>
         </div>
        <div className="post-container-header">
          <div className="Post-Header">
            <img
              src={miniava}
              width={50}
              height={50}
              alt="kekW"
            />
          </div>
          <div className="Post-Header-text">
            <a href="/im">{`${sername} ${name}`}</a>
            <p className="Post-date">{date}</p>
          </div>
          
        </div>
        <p>{body}</p>
      </div>
  );
};
