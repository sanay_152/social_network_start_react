import React from "react";
import styles from "./PostCreator.module.css";

export const PostCreator = (props) => {
  let textWriter = React.createRef();

  return (
    <div className={styles.textAreaContainer}>
      <form onSubmit={props.createPost} action="">
        <textarea
          className={styles.textareaPost}
          placeholder={props.placeholder}
          value={props.newPostText}
          onChange={() => props.updateText(textWriter.current.value)}
          ref={textWriter}
        ></textarea>
        <input type="submit" />
      </form>
      
    </div>
  );
};
