import React from "react";
import "./Dialogs.css";
import miniava from "../../../../img/ava.jpg";

export const Message = ({ author, date, text }) => {
  /* console.log(props); */
  return (
    <div className="messageItem">
      <div>
        <img src={miniava} width={50} height={50} alt="kekW" />
      </div>
      <div className='messageInfoContainer'>
        <span className="messageAuthor">{author}</span>
        <span className="messageDate">{date}</span>
        <p className="messageText">{text}</p>
      </div>
    </div>
  );
};
