import React from "react";
import { Message } from "./Message";
import "./Dialogs.css";

export const MessagesBox = (props) => {
  const allmessages = props.messages.map((message, ind) => (
    <Message author={message.author} date={message.date} text={message.text} key={ind}/>
  ));

  return <div className="message-container">{allmessages}</div>;
};
