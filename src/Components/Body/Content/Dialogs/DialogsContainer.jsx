import { connect } from "react-redux";
import {DialogsPage} from './DialogsPage'
import {unpdateNewMessageTextActionCreator, sendMessage} from '../../../../reduxStore/Dialogs-reduser'

let mapStateToprops = (state) => {
    return {
      messages: state.dialogsPage.messages,
      newMessageText: state.dialogsPage.newMessageText,
    };
  };
  
  let mapDispatchToProps = (dispatch) => {
    return {
      updateText: (value) => {
        dispatch(unpdateNewMessageTextActionCreator(value));
      },
      sendMessage: (e) => {
        e.preventDefault();
        dispatch(sendMessage());
      },
    };
  };

 export default connect(mapStateToprops, mapDispatchToProps)(DialogsPage)

