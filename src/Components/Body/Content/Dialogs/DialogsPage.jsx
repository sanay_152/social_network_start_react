import React from "react";
import { PostCreator } from "../../Post/PostCreator";
import { MessagesBox } from "./MessageBox";

export const DialogsPage = (props) => {

    

  return (
    <>
      <MessagesBox messages={props.messages} />
      <PostCreator
      placeholder='Напишите сообщение...'
        createPost={props.sendMessage}
        updateText={props.updateText}
        newPostText={props.newMessageText}
      />
    </>
  );
};
