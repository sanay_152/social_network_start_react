import React from "react";
import { StartPageHeader } from "./StartPageHeader/StartPageHeader";
import { StartPagePosts } from "./StartPageHeader/StartPagePosts";

import "./Content.css";

const data = {
  sername: "Кузьмич",
  name: "Александр",
  age: 20,
  study: "УрФУ им. Первого президента Бориса Ельцина",
  prof: "Web-developer",
  sity: "Екатеринбург",
};
const MyPage = (props) => {
    return (
      <div className="content-block">
        <StartPageHeader data={data} />
        <StartPagePosts posts={props.posts} newPostText={props.newPostText} unpdateText={props.unpdateText} createPost={props.createPost} deletePost={props.deletePost}/>
      </div>
    );
  }

export default MyPage;
