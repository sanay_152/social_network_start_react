import React from "react";
import { PostCreator } from "../../Post/PostCreator";
import { PostContainer } from "../../Post/PostContainer";

export const StartPagePosts = (props) => {
  return (
    <React.Fragment>
        <PostCreator
        placeholder="Что-нового?"
        createPost={props.createPost}
        updateText={props.unpdateText}
        newPostText={props.newPostText}
      />
      <PostContainer
        store={props.store}
        allPosts={props.posts}
        deletePost={props.deletePost}
      /> 
    </React.Fragment>
  );
};
