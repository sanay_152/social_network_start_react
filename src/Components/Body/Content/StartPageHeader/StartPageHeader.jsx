import React from "react";
import ava from '../../../../img/ava.jpg'

import "./StartPageHeader.css";

export const StartPageHeader = ({
  data: { sername, name, age, study, prof, sity },
  post,
}) => {
  return (
      <div className="StartPageHeader-content">
        <div className="Start-img">
          <img
            src={ava}
            alt=""
          />
          <input type="button" value="ВКонтакте" />
        </div>
        <div className="info">
          <ul className="StartPageHeader-about">
            <li>{`${sername} ${name}`}</li>
            <li>{age} лет</li>
            <li>{prof}</li>
            <li>{sity}</li>
            <li>{study}</li>
          </ul>
        </div>
      </div>
  );
};
