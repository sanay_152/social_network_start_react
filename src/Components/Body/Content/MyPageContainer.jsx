import MyPage from "./MyPage";
import { connect } from "react-redux";
import {
  unpdateNewPostTextActionCreator,
  createNewPostAC,
  deleteSelectedPostAC,
} from "../../../reduxStore/Profile-reducer";

let mapStateToprops = (state) => {
  return {
    posts: state.profilePage.posts,
    newPostText: state.profilePage.newPostText,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    unpdateText: (value) => {
      dispatch(unpdateNewPostTextActionCreator(value));
    },
    createPost: (e) => {
      e.preventDefault();
      dispatch(createNewPostAC());
    },
    deletePost: (e) => {
      dispatch(deleteSelectedPostAC(e.target.id));
    },
  };
};

export default connect(mapStateToprops, mapDispatchToProps)(MyPage);
