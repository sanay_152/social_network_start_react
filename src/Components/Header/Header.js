import React, { Component } from "react";
import "./Header.css";
import logo from './logo.jpg'
/* "https://sun3-10.userapi.com/7iyONqDOHZQMnohIZmVhVXbZ-yD8xPxWOkA_5g/KvHlhf-auZM.jpg */

class Header extends Component {
  
  
  render() {
   
    return (
      <div className="header">
        <div className="header-content">
          <div className="header-logo"><img src={logo} alt="sd"/></div>
          <div className="header-buttons">
            <input type="button" value="Регистрация" />
            <input type="button" value="Выход" />
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
