import axios from "axios";

const url = "https://covid19.mathdro.id/api";

export const fetchData = async (country) => {
  let changebleURL = url;
  if (country) {
    changebleURL = `${url}/countries/${country}`;
  }

  try {
    const {
      data: { confirmed, recovered, deaths, lastUpdate },
    } = await axios.get(changebleURL);

    return { confirmed, recovered, deaths, lastUpdate };
  } catch (err) {
    console.log(err);
  }
};
export const fetchDailyData = async () => {
  try {
    const { data } = await axios.get(`${url}/daily`);
    const modifiedData = data.map((DD) => ({
      confirmed: DD.confirmed.total,
      deaths: DD.deaths.total,
      recovered: DD.recovered.total,
      date: DD.reportDate,
    }));
    return modifiedData;
  } catch (err) {}
};
export const fetchTemperatureKost = async () => {
  try {
    const apiKey = "679d16f9db73dc323e7965750a0b5663";
    const { data } = await axios.get(
      `http://api.openweathermap.org/data/2.5/weather?q=Kostanai&appid=${apiKey}`
    );

    return data;
  } catch (err) {}
};
export const fetchTemperatureEkb = async () => {
  try {
    const apiKey = "679d16f9db73dc323e7965750a0b5663";
    const { data } = await axios.get(
      `http://api.openweathermap.org/data/2.5/weather?q=Ekaterinburg&appid=${apiKey}`
    );

    return data;
  } catch (err) {}
};
export const fetchTemperatureMsk = async () => {
  try {
    const apiKey = "679d16f9db73dc323e7965750a0b5663";
    const { data } = await axios.get(
      `http://api.openweathermap.org/data/2.5/weather?q=Moscow&appid=${apiKey}`
    );

    return data;
  } catch (err) {}
};
export const fetchcountries = async () => {
  try {
    const {
      data: { countries },
    } = await axios.get(`${url}/countries`);
    return countries.map((cou) => cou.name);
  } catch (err) {}
};
