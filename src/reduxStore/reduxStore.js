import { createStore, combineReducers } from "redux";
import { newPostAction } from "./Profile-reducer";
import {newDialogAction} from './Dialogs-reduser'

let reducers = combineReducers({ profilePage: newPostAction, dialogsPage:newDialogAction });

let store = createStore(reducers);

export default store;
