let initialState = {
    messages: [
      {
        author: "Кузьмич Александр",
        text: "Привет1",
        date: new Date().toUTCString(),
      },
      {
        author: "Кузьмич Александр",
        text: "Привет2",
        date: new Date().toUTCString(),
      }, {
        author: "Кузьмич Александр",
        text: "Привет3",
        date: new Date().toUTCString(),
      },
    ],
    newMessageText: "",
  };

  export const newDialogAction = (state = initialState, action) => {
    switch (action.type) {
      case "UPDATE_NEW_MESSAGE_TEXT":
        return {
          ...state,
          newMessageText: action.newText,
        };
  
      case "SEND_MESSAGE":
        let newMessage = {
          author: "Кузьмич Александр",
          text: state.newMessageText,
          date: new Date().toUTCString(),
        };
        return { ...state, messages: [...state.messages, newMessage], newMessageText: "" };
     
      default:
        return { ...state };
    }
  };
  
  export const unpdateNewMessageTextActionCreator = (text) => {
    return { type: "UPDATE_NEW_MESSAGE_TEXT", newText: text };
  };
  export const sendMessage = () => {
    return { type: "SEND_MESSAGE" };
  };