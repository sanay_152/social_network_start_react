let initialState = {
  posts: [
    {
      sername: "Кузьмич",
      name: "Александр",
      body: "Как прекрасно программировать в свободное время",
      date: new Date().toUTCString(),
      id: 0,
    },
    {
      sername: "Кузьмич",
      name: "Александр",
      body: "Привет, я делаю свою социальную сеть",
      date: new Date().toUTCString(),
      id: 1,
    },
  ],
  newPostText: "",
  postId: undefined,
};

export const newPostAction = (state = initialState, action) => {
  switch (action.type) {
    case "UPDATE_NEW_POST_TEXT":
      return {
        ...state,
        newPostText: action.newText,
      };

    case "ADD_POST":
      let newPost = {
        id: state.posts.length,
        sername: "Кузьмич",
        name: "Александр",
        body: state.newPostText,
        date: new Date().toUTCString(),
        likesCount: 0,
      };
      return { ...state, posts: [...state.posts, newPost], newPostText: "" };
    case "DELETE_POST":
      const newPostWithout = state.posts.filter(
        (elem) => +elem.id !== +action.postId
      );
      return { ...state, posts: [...newPostWithout] };
    default:
      return { ...state };
  }
};

export const unpdateNewPostTextActionCreator = (text) => {
  return { type: "UPDATE_NEW_POST_TEXT", newText: text };
};
export const createNewPostAC = () => {
  return { type: "ADD_POST" };
};
export const deleteSelectedPostAC = (id) => {
  return { type: "DELETE_POST", postId: id };
};
